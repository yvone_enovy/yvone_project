import Controller from '@ember/controller';

export default Controller.extend({
	newName: 'epic',
	llamaNames: [
		{
			name: 'Llamandarina',
			age: 9
		},
		{
			name: 'Llamapalooza',
			age: 10
		},
		{
			name: 'Hendrix',
			age: 67
		}
	],
	actions: {
		nameALlama() {
			var names = this.get('llamaNames');
			var result = Math.floor(Math.random() * names.get('length'));
			this.set('newName', names[result]['name']);
		}
	}
});
