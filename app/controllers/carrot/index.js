import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
		createMeal() {
			let name = this.get('newMeal');
			this.store.createRecord('meal', {name})
				.save()
				.then(() => {
					alert('New meal saved!')
				})
				.catch((e) => {
					alert(`Something failed while saving your meal. ${e}`)
				})
				.finally(() => {
					this.set('newMeal', null);
				})
		},

		uselessAction(name, text) {
			alert('Just an alert for ' + name + ' ' + text)
		}
	}
});
