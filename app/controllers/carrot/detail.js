import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
		updateMeal(meal){
			meal.save()
				.catch((error) => {
					alert('Ocurrió un error al guardar')
				});
		},

		deleteMeal(meal){
			meal.destroyRecord()
				.then( this.transitionToRoute('carrot.index') )
				.catch((error) => {
					alert('Ocurrió un error al eliminar')
				});
		},

		otraAccion(){
			console.log('holaaaa')
		}
	}
});
