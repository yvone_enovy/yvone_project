import Controller from '@ember/controller';

export default Controller.extend({
	cat: 'pelusa',
	weekDays: [
		{
			day: 'monday',
			number: 1,
			'mi propiedad': 'hola'
		},
		{
			day: 'tuesday',
			number: 2,
			'mi propiedad': 'hola'
		},
		{
			day: 'wednesday',
			number: 3,
			'mi propiedad': 'hola'
		},
		{
			day: 'thursday',
			number: 4,
			'mi propiedad': 'hola'
		},
		{
			day: 'friday',
			number: 5,
			'mi propiedad': 'hola'
		},
		{
			day: 'saturday',
			number: 6,
			'mi propiedad': 'hola'
		},
		{
			day: 'sunday',
			number: 7,
			'mi propiedad': 'hola'
		}
	],
	tweets: [],
	newDescription: 'wa',

	actions: {
		doSomething() {
			this.set('cat', 'perro');
			var cat = this.get('cat');
			// console.log(`the name is ${cat}`);
			alert('HOLA');
		},

		myLuckyDay(){
			var weekDays = this.get('weekDays');
			// var luckyDay = this.get('luckyDay');

			var n = Math.floor(Math.random() * weekDays.length);
			
			// console.log(weekDays.objectAt(n));
			// this.set('luckyDay', weekDays.objectAt(n)['day']);
			this.set('luckyDay', weekDays[n]['day']);
		},

		addDay(){
			var newDay = this.get('newDay');
			var weekDays = this.get('weekDays');

			weekDays.pushObject({
				day: newDay,
				number: Math.floor(Math.random() * 100),
				'mi propiedad': 'hola'
			});
		},

		addTweet(){
			var tweets = this.get('tweets');
			var newTweet = this.get('newTweet');

			tweets.pushObject({
				description: newTweet,
				user: 'Yvone',
				isDisabled: true
			});

			this.set('newTweet', null);
		},

		deleteTweet(tweet){
			var tweets = this.get('tweets');
			var result = confirm('deseas borrar este tweet: ' + tweet.description);
			if (result) {
				tweets.removeObject(tweet);
			}
		},

		createAuthor(){
			var firstName = this.get('name');
			var lastName = this.get('lastname');
			var nickname = this.get('nickname');
			var age = this.get('age');

			var newAuthor = this.store.createRecord('author', {firstName, lastName, nickname, age});
			newAuthor.save();
		}
	}
});
