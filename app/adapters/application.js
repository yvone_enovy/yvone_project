import ActiveModelAdapter from 'active-model-adapter';
//import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import config from '../config/environment';

export default ActiveModelAdapter.extend({
	host: config.host
	// host: 'https://still-garden-88285.herokuapp.com'
});