import Route from '@ember/routing/route';
//import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
// is where all session events are triggered and in some situations they are triggered automatically
// It automatically maps the session events to the sessionAuthenticated (will transition to a configurable route) 
// and sessionInvalidated (will reload the page to clear all the potentially sensitive data from memory) methods it implements

export default Route.extend({
});
