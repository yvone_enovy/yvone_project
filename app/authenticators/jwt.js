import Ember from 'ember';
import Base from 'ember-simple-auth/authenticators/base';
import { inject as service } from '@ember/service';
import config from '../config/environment';

const { RSVP: { Promise }, $: { ajax }, run } = Ember;

export default Base.extend({
  session: service(),
  tokenEndpoint: `${config.host}/sign_in`,
  restore(data) {
    return new Promise((resolve, reject) => {
      if (!Ember.isEmpty(data.token)) {
        this.get('session').pushCredentials(data);
        resolve(data);
      } else {
        reject();
      }
    });
  },
  authenticate(creds) {
    const { username, password } = creds;
    const data = JSON.stringify({ username, password });
    const requestOptions = {
      url: this.tokenEndpoint,
      type: 'POST',
      data,
      contentType: 'application/json',
      dataType: 'json'
    };
    return new Promise((resolve, reject) => {
      ajax(requestOptions).then((response) => {
        const { jwt, user } = response;
        this.get('session').pushCredentials({ user })
        // Wrapping async operation in Ember.run
        run(() => {
          resolve({
            token: jwt,
            user
          });
        });
      }, (error) => {
        // Wrapping async operation in Ember.run
        run(() => {
          reject(error);
        });
      });
    });
  },
  invalidate(data) {
    return Promise.resolve(data);
  }
});