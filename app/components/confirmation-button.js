import Component from '@ember/component';

export default Component.extend({
	actions: {
		launchConfirmDialog() {
			this.set('confirmShown', true);
		},

		submitConfirm() {
			this.onConfirm()
				.then( this.set('confirmShown', false) )
		},

		cancelConfirm() {
			this.set('confirmShown', false);
		}
	}
});
