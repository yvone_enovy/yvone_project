import Component from '@ember/component';

export default Component.extend({
	isConfirming: false,

	actions: {
		confirmAction() {
			this.set('isConfirming', true)
			this.onConfirm()
				.then(() => {
					this.set('isConfirming', false)
				})
		}
	}
});
