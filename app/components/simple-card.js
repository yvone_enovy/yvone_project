import Component from '@ember/component';

export default Component.extend({
	tagName: 'p',
	classNames: ['primary'],
	classNameBindings: ['isOld:old'],
	isOld: false
})
// .reopenClass({
// 	positionalParams: ['title', 'text', 'otherText']
// });
